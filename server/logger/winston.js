const winston = require('winston')

const levels = {
    error: 0,
    warn: 1,
    info: 2,
    verbose: 3,
    debug: 4,
    silly: 5
}

const loggerMiddleWare = winston.createLogger({
    level: 'info',
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: 'detail.log' })
    ]
})

logger.info('Hello World!!')

module.exports = loggerMiddleWare