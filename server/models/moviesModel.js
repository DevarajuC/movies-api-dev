const { Pool } = require(`pg`);

const pool = new Pool({
    user: process.env.PGUSER,
    password: process.env.PGPASSWORD,
    database: process.env.PGDATABASE,
    port: process.env.PGPORT,
    host: process.env.PGHOST
});

const getMovies = () => {
    return new Promise((resolve, reject) => {
        pool
            .query(`SELECT * FROM movies`)
            .then(result => resolve(result))
            .catch(error => reject(error));
    });
};

const getMoviesId = rank => {
    return new Promise((resolve, reject) => {
        pool
            .query(`SELECT * FROM movies WHERE rank = ${rank}`)
            .then(result => resolve(result))
            .catch(error => reject(error));
    });
};

const setMoviesById = (
    rank,
    title,
    description,
    runtime,
    genre,
    rating,
    metascore,
    votes,
    gross_earning_in_mil,
    directorid,
    actor,
    year
) => {
    return new Promise((resolve, reject) => {
        pool
            .query(
                `INSERT INTO movies VALUES(${rank},'${title}','${description}',
                ${runtime},'${genre}',${rating},'${metascore}','${votes}',
                '${gross_earning_in_mil}',
                (SELECT id FROM director WHERE dname = '${directorid}'),
                '${actor}',${year})`
            )
            .then(result => resolve(result))
            .catch(error => reject(error));
    });
};

const updateMovies = (
    rank,
    title,
    description,
    runtime,
    genre,
    rating,
    metascore,
    votes,
    gross_earning_in_mil,
    actor,
    year
) => {
    return new Promise((resolve, reject) => {
        pool
            .query(
                `UPDATE movies SET rank=${rank},description='${description}',
            runtime=${runtime},genre='${genre}',rating=${rating},metascore='${metascore}',
            votes='${votes}',gross_earning_in_mil='${gross_earning_in_mil}',actor='${actor}',
            year=${year} WHERE title='${title}'`
            )
            .then(result => resolve(result))
            .catch(error => reject(error));
    });
};

const deleteMovies = rank => {
    return new Promise((resolve, reject) => {
        pool
            .query(`DELETE FROM movies WHERE rank = ${rank}`)
            .then(result => resolve(result))
            .catch(error => reject(error));
    });
};

module.exports = {
    getMovies,
    getMoviesId,
    setMoviesById,
    updateMovies,
    deleteMovies
};