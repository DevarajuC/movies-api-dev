const { Pool } = require(`pg`);

const pool = new Pool({
    user: process.env.PGUSER,
    password: process.env.PGPASSWORD,
    database: process.env.PGDATABASE,
    port: process.env.PGPORT,
    host: process.env.PGHOST
});

const getDirector = () => {
    return new Promise((resolve, reject) => {
        pool
            .query(`SELECT * FROM director`)
            .then(result => resolve(result))
            .catch(error => reject(error));
    });
};

const getDirectorById = directorId => {
    return new Promise((resolve, reject) => {
        pool
            .query(`SELECT * FROM director WHERE id = ${directorId}`)
            .then(result => resolve(result))
            .catch(error => reject(error));
    });
};

const setDirector = (directorId, dname) => {
    return new Promise((resolve, reject) => {
        pool
            .query(`INSERT INTO director VALUES(${directorId},'${dname}')`)
            .then(result => resolve(result))
            .catch(error => reject(error));
    });
};

const updateDirector = (id, dname) => {
    return new Promise((resolve, reject) => {
        pool
            .query(`UPDATE director SET dname = '${dname}' WHERE id = ${id}`)
            .then(result => resolve(result))
            .catch(error => reject(error));
    });
};
//
const deleteDirector = directorId => {
    return new Promise((resolve, reject) => {
        pool
            .query(`DELETE FROM director WHERE id = ${directorId}`)
            .then(result => resolve(result))
            .catch(error => reject(error));
    });
};
//

module.exports = {
    getDirector,
    getDirectorById,
    setDirector,
    updateDirector,
    deleteDirector
};