const joi = require(`@hapi/joi`);

const schema = joi.object({
    RANK: joi.number().integer(),
    TITLE: joi.string(),
    DESCRIPTION: joi.string(),
    RUNTIME: joi.number().integer(),
    GENRE: joi.string(),
    RATING: joi.number().precision(2),
    METASCORE: joi.string(),
    VOTES: joi.number().integer(),
    GROSS: joi.string(),
    DIRECTORID: joi.number().integer(),
    DIRECTORNAME: joi.string(),
    ACTOR: joi.string(),
    YEAR: joi.number().integer()
});

module.exports = schema;