const express = require(`express`)
const app = express()
const bodyParser = require(`body-parser`)
const { morganMiddleWare } = require(`./logger/morgan.js`)
    // const { loggerMiddleWare } = require(`./logger/winston.js`)
const port = process.env.PORT

app.use(morganMiddleWare)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(`/api/director`, require(`./controller/director.js`))
app.use(`/api/movies`, require(`./controller/movies.js`))

app.listen(port, () => console.log(`Server connected on port: ${port}`))