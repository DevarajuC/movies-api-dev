const express = require(`express`);
const schema = require(`./../validation/mValidation.js`);
const router = express.Router();
const {
    getMovies,
    getMoviesId,
    setMoviesById,
    updateMovies,
    deleteMovies
} = require(`./../models/moviesModel.js`);

router.get(`/all`, async(req, res) => {
    try {
        const result = await getMovies();
        console.table(result.rows);
        res.status(200).send(result.rows);
    } catch (error) {
        console.error(error.message);
        res.status(404).send(`ID Not Found`);
    }
});

router.get(`/:rank`, async(req, res) => {
    try {
        await schema.validateAsync({ RANK: req.params.rank });
        const result = await getMoviesId(req.params.rank);
        if (result.rows.length === 0) {
            console.log(`Movie Not Available!!`);
            res.status(404).send(`Movie Not Available!!`);
        } else {
            console.table(result.rows);
            res.status(200).send(result.rows);
        }
    } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
    }
});

router.post(`/insert`, async(req, res) => {
    try {
        await schema.validateAsync({
            RANK: req.body.rank,
            TITLE: req.body.title,
            DESCRIPTION: req.body.description,
            RUNTIME: req.body.runtime,
            GENRE: req.body.genre,
            RATING: req.body.rating,
            METASCORE: req.body.metascore,
            VOTES: req.body.votes,
            GROSS: req.body.gross_earning_in_mil,
            DIRECTORNAME: req.body.directorid,
            ACTOR: req.body.actor,
            YEAR: req.body.year
        });
        const result = await setMoviesById(
            req.body.rank,
            req.body.title,
            req.body.description,
            req.body.runtime,
            req.body.genre,
            req.body.rating,
            req.body.metascore,
            req.body.votes,
            req.body.gross_earning_in_mil,
            req.body.directorid,
            req.body.actor,
            req.body.year
        );
        console.log(`${result.rowCount} Row Inserted successfully`);
        res.status(201).send(`${result.rowCount} Row Inserted successfully`);
    } catch (error) {
        console.error(error.message);
        res.status(405).send(error.message);
    }
});

router.put(`/update`, async(req, res) => {
    try {
        await schema.validateAsync({
            RANK: req.body.rank,
            TITLE: req.body.title,
            DESCRIPTION: req.body.description,
            RUNTIME: req.body.runtime,
            GENRE: req.body.genre,
            RATING: req.body.rating,
            METASCORE: req.body.metascore,
            VOTES: req.body.votes,
            GROSS: req.body.gross_earning_in_mil,
            ACTOR: req.body.actor,
            YEAR: req.body.year
        });
        const result = await updateMovies(
            req.body.rank,
            req.body.title,
            req.body.description,
            req.body.runtime,
            req.body.genre,
            req.body.rating,
            req.body.metascore,
            req.body.votes,
            req.body.gross_earning_in_mil,
            req.body.actor,
            req.body.year
        );
        if (result.rowCount === 0) {
            console.log(`Movie Not Present!!`);
            res.status(404).send(`Movie Not Present!!`);
        } else {
            console.log(`${result.rowCount} Row Updated successfully`);
            res.status(200).send(`${result.rowCount} Row Updated successfully`);
        }
    } catch (error) {
        console.error(error.message);
        res.status(405).send(error.message);
    }
});

router.delete(`/delete/:rank`, async(req, res) => {
    try {
        await schema.validateAsync({ RANK: req.params.rank });
        const result = await deleteMovies(req.params.rank);
        if (result.rowCount === 0) {
            console.log(`Movie Not Present!!`);
            res.status(404).send(`Movie Not Present!!`);
        } else {
            console.log(`${result.rowCount} Row Deleted successfully`);
            res.status(410).send(`${result.rowCount} Row Deleted successfully`);
        }
    } catch (error) {
        console.error(error.message);
        res.status(405).send(error.message);
    }
});

module.exports = router;